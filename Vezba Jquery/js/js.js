++
// shorter version of $(document).ready(function()
// $(function() {


// });


$(document).ready(function() {
    console.log("document ready function");
    
    // $("div").addClass("colorized").hide();
    // jQuery("section").addClass("colorized");
    
    // $("div").addClass("new-color");
    $(".colorized").show();

    function showSendBtnWhenAllInputsHaveValues(){
        const numberOfInputs = $(`#contact-form input`).length;
        let numberOfFieldInputs = 0
        $(`#contact-form input`).each(function(index, element) {
            if (element.value != "") {
                numberOfFieldInputs++;

                if (numberOfInputs == numberOfFieldInputs) {
                    $(`#send-btn`).slideDown();   
                }
            }
        })
    }
    function clearContactForm() {
        $("#contact-form input").val("");
    }

    $(`#contact-btn`).on(`click`, function () {
        $(`#contact-form`).fadeIn(800);
    })

    $(`#contact-form input`).on(`blur`, function () {
        showSendBtnWhenAllInputsHaveValues(); 
    })

    $(`#send-btn`).on(`click`, function (e){
        e.preventDefault();
        $(`#loading-img`).fadeIn(800);
        $(this).fadeOut();

        $(`#loading-img`).fadeOut(300);
        $(this).fadeIn(3100, function () {
            clearContactForm();
        });
    })

    // animation section
    $(`#start-btn`).on('click', function() {
        $(`.box:eq(0)`).fadeIn(2000, function() {
            $(`.box:eq(1)`).fadeIn(2000, function() {
                $(`.box:eq(2)`).fadeIn(2000);
            });
        })
    })


    $("#search-btn").on("click", function() {
        // if($("search-input").css("display") == "none") {
        //     $("#search-input").fadeIn(2000);
        // }
        // else {
        //     $("#search-input").fadeOut(2000);
        // }

        $(".search-input").toggleClass("active-input");
    })

    $(".check-btn").on("click", function(){

        
        $("div.child").toggleClass(function(){
            if($(this).parent().is(".parent")) {
                return "has-parent";
            }
            else {
                return "doesnt-have-parent";
            }
        });
        
    })
});