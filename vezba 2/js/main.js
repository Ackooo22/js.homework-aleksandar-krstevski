function myFunction() {

    console.log("Hello World")
}

myFunction();

// functions with parameters

function myFunctionWithParameters(x,y) {
    let myVar = x * y;
    console.log(myVar);

} 

myFunctionWithParameters(8,5);


function myFunctionWithParameters2(x,y) {
    let myVar = x * y;

    return myVar;
}
myFunctionWithParameters2(8,2);

let result = myFunctionWithParameters2(8,2);
console.log(result);

function fullName(firstName, lastName) {
    return firstName + " " + lastName;
}

let firstName = "Aleksandar";
let lastName = "Krstevski";

let fullNameResult = fullName(firstName, lastName);

console.log(fullNameResult);

function calculate(x,y) {
    let myVar = x + y;

    return myVar;
}

let results = calculate(6,80);

console.log(results);


// temp conversion

function celsiusToFahrenheit(temp) {
    let fahrenheit = (temp * 9) / 5 + 32;
    return `${temp}C is ${fahrenheit}F`;
}

console.log(celsiusToFahrenheit(16));

function ferToCel(temp) {
    let celsius = ((temp - 32)*5)/9;
    return `${temp}F is ${celsius}C`;
}

console.log(ferToCel(80));

function fahrenheitToCelsius2(temp) {
    let fahrenheit = temp;
    let celsius = ((fahrenheit - 32)*5)/9;
    return `${fahrenheit}F is ${celsius}C`;
}

console.log(fahrenheitToCelsius2(24));

function fortuneTeller(job, location, partner, kids){
    return `You have a position at ${job} in ${location} with ${partner} and ${kids}`;
}

let fortune1 = fortuneTeller(`McDonalds`, `Newyork`,`Jess`,`Mia`);
console.log(fortune1);


let number = 14;

if (typeof(number) == "number"){
    console.log("the variable is a number" ,number);
}


let score = 50;

if (score > 100) {
    alert(`You won!`);
}
else if (score < 100) {
    alert(`You lose!`);
}
else {
    alert(`Its a tie!`);
}


let money = prompt(`How much money do you have?`);

if (money >= 200) {
    console.log(`You should invest in tesla!`);
}
else if (money <= 50) {
    console.log(`You should survive till you arent broke mf!`);
}

function greaterNumber(num1, num2){
    let max;
    if (num1 > num2) {
        max = num1;
    }
    else {
        max = num2;
    }

    return `the greater number of ${num1} and ${num2} is`;
}

console.log(greaterNumber(10, 21));

function assignGrade(grade){
    if (grade >= 70 && grade <= 100) {
        return `A`;
    }
    else if (grade >= 60 && grade <= 69) {
        return `B`;
    }
    
    else if (grade >= 50 && grade <= 59) {
        return `C`;
    }
    
    else if (grade >= 40 && grade <= 49) {
        return `D`;
    }
    
    else if (grade >= 30 && grade <= 49) {
        return `E`;
    }
    
    
    else if (grade >= 10 && grade <= 29) {
        return `F`;
    }
}

let grade = assignGrade(63);

console.log(grade);

function helloWorld(language) {
    if (language == "de") {
      return "Hello World DE";
    } else if (language == "es") {
      return "Hello World ES";
    } else {
      return "Hello World";
    }
  }
  
  console.log(helloWorld(""));
    


  function chineseZodiac(year){
    let result = (year - 4) % 12;

    switch(result) {
        case 0:
            console.log("Rat");
            break;
            case 1:
            console.log("Ox");
            break;
            case 2:
            console.log("Tiger");
            break;
            case 3:
            console.log("Rabbit");
            break;
            case 4:
            console.log("Dragon");
            break;
            case 5:
            console.log("Snake");
            break;
            case 6:
            console.log("Horse");
            break;
            case 7:
            console.log("Goat");
            break;
            case 8:
            console.log("Monkey");
            break;
            case 9:
            console.log("Rooster");
            break;
            case 10:
            console.log("Dog");
            break;
            case 11:
            console.log("Pig");
            break;
            default:
                console.log("Invalid");
                break;
    }
}

chineseZodiac(1999);