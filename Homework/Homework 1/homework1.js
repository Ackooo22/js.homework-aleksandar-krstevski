//homework part 1
document.querySelector("#myTitle").textContent = "Changed";

document.querySelector(".paragraph").textContent = "Changed";

document.querySelector(".second").textContent = "Changed";

document.querySelector("text").textContent = "Changed";

let changedHeader = document.getElementsByTagName("h1")[1];

changedHeader.textContent = "Changed";

document.querySelector("h3").textContent = "Changed";

//homework part 2
let numbers = [1, 2, 2, 4, 5, 6];
let arraySum = 0;

const listDiv = document.querySelector(".homework-part-2");

let ulElement = document.createElement("ul");

listDiv.appendChild(ulElement);

for (let i = 0; i < numbers.length; i++) {
  let liElement = document.createElement("li");
  liElement.textContent = numbers[i];
  ulElement.appendChild(liElement);

  arraySum += numbers[i];
  console.log(arraySum);
}

document.querySelector(".sum").textContent = `The sum is ${arraySum}`;
document.querySelector(".equetion").textContent = `( 2 + 4 + 5 = 11)`;
