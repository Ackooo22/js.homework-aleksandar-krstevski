/*Homework 2 part 1 */
const btn1 = document.querySelector(".submit");
const table = document.querySelector("table");

btn1.addEventListener("click", function () {
  let firstName = document.getElementById("firstName").value;
  let lastName = document.getElementById("lastName").value;
  let email = document.getElementById("email").value;
  let password = document.getElementById("password").value;

  let addTR = document.createElement("tr");
  table.appendChild(addTR);

  let firstNameTD = document.createElement("td");
  firstNameTD.innerText = firstName;
  addTR.appendChild(firstNameTD);

  let lastNameTD = document.createElement("td");
  lastNameTD.innerText = lastName;
  addTR.appendChild(lastNameTD);

  let emailTD = document.createElement("td");
  emailTD.innerText = email;
  addTR.appendChild(emailTD);

  let passwordTD = document.createElement("td");
  passwordTD.innerText = password;
  addTR.appendChild(passwordTD);
});

/*Homework 2 part 2 */

const btn2 = document.querySelector(".create");
const forTable = document.querySelector(".forTable");

btn2.addEventListener("click", function () {
  const numOfColumns = document.querySelector("#columns").value;
  const numOfRows = document.querySelector("#rows").value;

  for (i = 1; i <= numOfColumns; i++) {
    let createColumn = document.createElement("tr");
    forTable.appendChild(createColumn);
    for (j = 1; j <= numOfRows; j++) {
      let createRow = document.createElement("td");
      createRow.textContent = `Column${j} Row${j}`;
      createColumn.appendChild(createRow);
    }
    forTable.appendChild(createColumn);
  }
});
