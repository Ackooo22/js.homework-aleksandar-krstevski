 //define my school

function School(name) {
  this.name = name;
  this.students = [];

  //add Student
  this.addStudent = function (student) {
    this.students.push(student);
  };

  // List/Print Students
  this.listStudents = function () {
    const studentList = document.querySelector("#student-list");

    let htmlToAdd = "";

    const deleteBtn = `<button class="delete-btn">Delete</button>`;
    for (let student of this.students) {
      htmlToAdd += `<li id="${student.index}" data-index="${student.index}">  
            ${student.name} ${student.surname}, ${student.index}, ${student.email} ${deleteBtn}
        </li>`;
    }

    studentList.innerHTML = htmlToAdd;
  };

  // delete/expell a student
  this.deleteStudent = function (index) {
    const student = this.students.find((element) => element.index === index);
    if (student) {
      const position = this.students.indexOf(student);
      this.students.splice(position, 1);
      this.listStudents();
    }
  };

  // search student by name
  this.searchByName = function (name) {
    const student = this.students.find((element) => element.name === name);
    if (student) {
      const element = document.querySelector("#search-list");
      element.innerHTML += `<li> ${student.name}, ${student.index} <a href="mailto:${student.email}">Send Mail</a> </li>`;
    }
  };
}

//define a student
function Student(name, surname, email, index, subjects) {
  this.name = name;
  this.surname = surname;
  this.email = email;
  this.index = index;
  this.subjects = subjects;
}

const mySchool = new School("SEDC");

const addBtn = document.querySelector("#add-btn");
addBtn.addEventListener("click", function (e) {
  e.preventDefault();
  const firstName = document.querySelector("#firstName").value;
  const lastName = document.querySelector("#lastName").value;
  const index = document.querySelector("#index").value;
  const email = document.querySelector("#email").value;
  const subjects = document.querySelector("#subjects").value;

  mySchool.addStudent(new Student(firstName, lastName, email, index, subjects));
  mySchool.listStudents();
});


// event listioner for deleting buttons

document.addEventListener('click', function(e) {
  if(e.target.classList.contains('delete-btn')) {
    const index =(e.target.closest('li').getAttribute("data-index"));
    mySchool.deleteStudent(index);
  }
})

// event listener for search input

const searchInput = document.querySelector('#search');
searchInput.addEventListener ('input', function () {
  // console.log('Recorded an input');

  const name = searchInput.value;
  mySchool.searchByName(name);
})