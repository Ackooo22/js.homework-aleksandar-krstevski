function Carlot(name) {
    this.name = name;
    this.cars = [];

    // write a method wheer it will take a string of object items and display them in our website

    // carAddList => this.cars.push(car);
    function carAddList(car) {
        this.cars.push(car);
    }

    // write a method where it will list and print the value of the object items

    function carListPrint() {
        const carList = document.getElementById('car-list')

        let htmlToAdd = ""

        const btns = `<div><button id="del-btn">Delete</button> <br> <button id="cal-btn">Calculate</button></div>`

        for(let car of this.cars) {
            htmlToAdd += `<li> ${car.model}, ${car.year}, ${car.color}, ${car.fuel}, 'Fuel Consumption is:' ${car.fuelConsumption}, ${car.distance} ${btns}</li>`
        }

        carList.innerHTML = htmlToAdd

    }

    function Car(model, year, color, fuel, fuelConsumption, distance) {
        this.model = model;
        this.year = year;
        this.color = color;
        this.fuel = fuel;
        this.fuelConsumption = fuelConsumption;
        this.distance = distance
    }

    const myCarLot = new Carlot('Alex car lot');

    // give the button a function that makes the values of all the inputs be displayed in car-list

    const addBtn = document.querySelector('#add-btn');
    addBtn.addEventListener('click', function(e) {
        e.preventDefault();
        const model = document.querySelector('#model').value;
        const year = document.querySelector('#year').value;
        const color = document.querySelector('#color').value;
        const fuel = document.querySelector('#fuel').value;
        const fuelConsumption = document.querySelector('#fuel-consumption').value;
        const distance = document.querySelector('#distance').value;

        myCarLot.carAddList(new Car(model, year, color, fuel, fuelConsumption, distance))
        myCarLot.carListPrint();
    })
}