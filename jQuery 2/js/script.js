$(function() {
    console.log("ready")

    $(`input[type="text"]`).on(`focus blur`, function() {
        console.log("The user focused or blurred the input");
    })

    $(window).on("resize scroll", function() {
        console.log("The window has been resized or scrolled");
    });

    let handleClick = function() {
        console.log("The user clicked");
    }

    $("input").on("click", handleClick);
    $("#click").on("click", handleClick);

    $("a").on('click', function(event){
        event.preventDefault();
        alert("the page didnt reload")
    })
})